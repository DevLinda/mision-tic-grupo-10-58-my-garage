/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupo10.mygarage.dao;

import com.grupo10.mygarage.models.Vehiculo;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author netoj
 */
public interface VehiculoDao extends CrudRepository<Vehiculo, String> {

    
}
