/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupo10.mygarage.models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author netoj
 */
@Table( name="Usuario" )
@Entity
@Getter
@Setter

public class Usuario implements Serializable{
    
    @Id
    @GeneratedValue( strategy= GenerationType.IDENTITY )
    @Column(name="idUsuario")
    private String idUsuario;
    
    @Column(name="Nombre")
    private String Nombre;
    
    @Column(name="Apellido")
    private String Apellido;
    
    
    @Column(name="Cargo")
    private String Cargo;
    
    
    
}
