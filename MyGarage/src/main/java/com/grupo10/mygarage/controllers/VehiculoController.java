/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupo10.mygarage.controllers;

import com.grupo10.mygarage.models.Vehiculo;
import com.grupo10.mygarage.services.VehiculoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author netoj
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/vehiculo")
public class VehiculoController {
    
    @Autowired
    private VehiculoService vehiculoService;
    
    @GetMapping(value = "/List")
    public List<Vehiculo> ConsultarVehiculos(){
        return vehiculoService.findAll();
    
}
    
}
