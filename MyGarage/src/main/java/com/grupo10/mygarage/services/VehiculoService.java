/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupo10.mygarage.services;

import com.grupo10.mygarage.models.Alquiler;
import com.grupo10.mygarage.models.Vehiculo;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author netoj
 */
@Service

public interface VehiculoService {
    public Vehiculo save(Vehiculo vehiculo);
    public void delete (String id);
    
    public Vehiculo finByName(String Propietario);
    public List<Vehiculo> findAll();   
}
