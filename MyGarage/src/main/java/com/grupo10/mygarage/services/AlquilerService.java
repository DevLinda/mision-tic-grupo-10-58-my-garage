/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupo10.mygarage.services;

import com.grupo10.mygarage.models.Alquiler;
import com.grupo10.mygarage.models.Plaza;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author netoj
 */

public interface AlquilerService {
    public Alquiler save(Alquiler alquiler);
    public void delete (Integer id);
    
    public Alquiler finById(Integer Id);
    public List<Alquiler> findAll();
}
