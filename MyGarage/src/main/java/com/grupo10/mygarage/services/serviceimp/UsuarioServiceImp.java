/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupo10.mygarage.services.serviceimp;

import com.grupo10.mygarage.dao.UsuarioDao;
import com.grupo10.mygarage.models.Usuario;
import com.grupo10.mygarage.services.UsuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author netoj
 */
@Service
public class UsuarioServiceImp implements UsuarioService {
    UsuarioDao usuarioDao;
    @Autowired

    @Override
    public Usuario save(Usuario usuario) {
        return usuarioDao.save(usuario);
    }

    @Override
    public void delete(String id) {
        usuarioDao.deleteById(id);
    }

    @Override
    public Usuario finById(String Id) {
        return usuarioDao.findById(Id).orElse(null);
    }

    @Override
    public List<Usuario> findAll() {
        return (List<Usuario>) usuarioDao.findAll();
    }
    
}
