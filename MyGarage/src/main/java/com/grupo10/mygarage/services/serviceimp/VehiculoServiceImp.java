/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupo10.mygarage.services.serviceimp;

import com.grupo10.mygarage.dao.VehiculoDao;
import com.grupo10.mygarage.models.Alquiler;
import com.grupo10.mygarage.models.Vehiculo;
import com.grupo10.mygarage.services.VehiculoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author netoj
 */
@Service
public class VehiculoServiceImp implements VehiculoService{
    @Autowired
    VehiculoDao vehiculoDao;

    @Override
    public Vehiculo save(Vehiculo vehiculo) {
        return vehiculoDao.save(vehiculo);
    }

    @Override
    public void delete(String id) {
        vehiculoDao.deleteById(id);
    }

    @Override
    public Vehiculo finByName(String Propietario) {
       return vehiculoDao.findById(Propietario).orElse(null);
    }

    @Override
    public List<Vehiculo> findAll() {
        return (List<Vehiculo>) vehiculoDao.findAll();
    }
    
    
}
