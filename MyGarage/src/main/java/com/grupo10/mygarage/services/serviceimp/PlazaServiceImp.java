/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupo10.mygarage.services.serviceimp;

import com.grupo10.mygarage.dao.PlazaDao;;
import com.grupo10.mygarage.models.Alquiler;
import com.grupo10.mygarage.models.Plaza;
import com.grupo10.mygarage.services.PlazaService;
;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.grupo10.mygarage.services.PlazaService;
;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author netoj
 */
@Service
public class PlazaServiceImp implements PlazaService {
    PlazaDao plazaDao;
    @Autowired
    

    @Override
    public Plaza save(Plaza plaza) {
        return plazaDao.save(plaza);
    }

    @Override
    public void delete(Integer id) {
        plazaDao.deleteById(id);
    }

    @Override
    public Plaza finById(Integer Id) {
        return plazaDao.findById(Id).orElse(null);
    }

    @Override
    public List<Plaza> findAll() {
        return (List<Plaza>) plazaDao.findAll();
    }


}
