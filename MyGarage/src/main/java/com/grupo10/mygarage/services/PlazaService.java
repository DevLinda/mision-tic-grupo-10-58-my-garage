/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupo10.mygarage.services;

import com.grupo10.mygarage.models.Plaza;
import com.grupo10.mygarage.models.Usuario;
import java.util.List;

/**
 *
 * @author netoj
 */
public interface PlazaService {
    public Plaza save(Plaza plaza);
    public void delete (Integer id);
    
    public Plaza finById(Integer Id);
    public List<Plaza> findAll();
}
