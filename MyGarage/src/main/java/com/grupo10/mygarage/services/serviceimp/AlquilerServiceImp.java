/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupo10.mygarage.services.serviceimp;

import com.grupo10.mygarage.dao.AlquilerDao;
import com.grupo10.mygarage.models.Alquiler;
import com.grupo10.mygarage.services.AlquilerService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author netoj
 */
@Service
public class AlquilerServiceImp implements AlquilerService{
    @Autowired
    AlquilerDao alquilerDao;

    @Override
    public Alquiler save(Alquiler alquiler) {
        return alquilerDao.save(alquiler);
    }

    @Override
    public void delete(Integer id) {
        alquilerDao.deleteById(id);
    }

    @Override
    public Alquiler finById(Integer Id) {
        return alquilerDao.findById(Id).orElse(null);
    }

    @Override
    public List<Alquiler> findAll() {
        return (List<Alquiler>) alquilerDao.findAll();
    }
    
}
