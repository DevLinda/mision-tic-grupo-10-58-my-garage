/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupo10.mygarage.services;

import com.grupo10.mygarage.models.Usuario;
import java.util.List;

/**
 *
 * @author netoj
 */

public interface UsuarioService {
    public Usuario save(Usuario usuario);
    public void delete (String id);
    
    public Usuario finById(String Id);
    public List<Usuario> findAll();
    
    
}
